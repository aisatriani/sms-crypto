package com.example.ruli.sms_kripto;

import com.example.ruli.sms_kripto.adapter.VigenereCipherAdapter;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    //@Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testDoublencrypte(){

        String keyApp = "123app";
        String keyPesan = "234msg";

        VigenereCipherAdapter chipher = new VigenereCipherAdapter();
        String encrypt = chipher.encrypt("ini pesan "+ keyPesan, keyApp);
        String decrypt = chipher.decrypt(encrypt, keyApp);
        assertEquals(encrypt,keyApp);

    }
}