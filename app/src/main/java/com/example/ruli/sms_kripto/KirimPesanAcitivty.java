package com.example.ruli.sms_kripto;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ruli.sms_kripto.adapter.VigenereCipherAdapter;

public class KirimPesanAcitivty extends AppCompatActivity {
    private EditText txtNomor, isi;
    private ImageView btnKirim;
    private TextView txtNama;
    private static final int PICK_CONTACT = 2016;
    private int STATUS = 1;
    private String KUNCI;

    private VigenereCipherAdapter vigenereCipherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kirim_pesan_acitivty);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vigenereCipherAdapter = new VigenereCipherAdapter();

        txtNomor = (EditText) findViewById(R.id.txtNumberPesan);
        txtNama = (TextView) findViewById(R.id.txtNamaPesan);
        btnKirim = (ImageView) findViewById(R.id.imgTulisPesan);
        isi = (EditText) findViewById(R.id.txtTulisPesan);

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isi.length() > 0) {
                    kirimPesan(txtNomor.getText().toString(), isi.getText().toString());
                    isi.setText(null);
                } else {
                    Toast.makeText(getBaseContext(), "Masukan Pesan Anda !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_kontak, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_kontak) {
            Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(i, PICK_CONTACT);
            return true;
        }else if (id == R.id.action_enkrip) {
            if (STATUS == 0){
                dialogKunci(item);
            }else{
                Toast.makeText(getApplicationContext(), "Pesan Tidak Di Enkripsi !", Toast.LENGTH_LONG).show();
                item.setIcon(R.mipmap.ic_open);
                STATUS = 0;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactUri = data.getData();
            Cursor cursor = getContentResolver().query(contactUri, null, null, null, null);
            if (cursor.moveToFirst()) {
                int number = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                txtNomor.setText(cursor.getString(number));
                txtNama.setText(cursor.getString(name));
            }
        }
    }

    private void kirimPesan(String phoneNumber, String message){
        String nMessage = null;
        if (STATUS == 1){

            message = Pref.CODE_TAG_ENC + message.toLowerCase();
            nMessage = vigenereCipherAdapter.encrypt(message, Pref.KEY_SMS);


        }else{
            nMessage = message;
        }
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "Pesan Terkirim", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "Pesan Di Terima", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "Pesan Tidak Di Terima", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, nMessage, sentPI, deliveredPI);

        goToPercakapan();

    }

    private void dialogKunci(final MenuItem item){
        final Dialog dialog = new Dialog(KirimPesanAcitivty.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_kunci);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        final EditText key = (EditText) dialog.findViewById(R.id.txtDialogKunci);
        Button ok = (Button) dialog.findViewById(R.id.btnDialogOK);
        Button cancel = (Button) dialog.findViewById(R.id.btnDialogCancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Masukan Kunci Anda !", Toast.LENGTH_SHORT).show();
                }else{
                    KUNCI = key.getText().toString();
                    STATUS = 1;
                    item.setIcon(R.mipmap.ic_lock);
                    Toast.makeText(getApplicationContext(), "Pesan Akan Di Enkripsi !", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void goToPercakapan(){
        Intent intent = new Intent(KirimPesanAcitivty.this, PercakapanActivity.class);
        intent.putExtra("id_thread", "");
        intent.putExtra("pengirim", txtNama.getText().toString());
        intent.putExtra("nomor", txtNomor.getText().toString());
        startActivity(intent);
    }
}
