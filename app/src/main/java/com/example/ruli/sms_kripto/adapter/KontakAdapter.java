package com.example.ruli.sms_kripto.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.widget.SimpleCursorAdapter;

import com.example.ruli.sms_kripto.R;

import java.io.File;
import java.io.FileOutputStream;


public class KontakAdapter extends AsyncTask<Void, Void, Cursor> {
    private MatrixCursor mMatrixCursor;
    private SimpleCursorAdapter mAdapter;
    private Activity activity;
    private String status;
    private Uri contactsUri;
    private Cursor contactsCursor;
    private ProgressDialog progressDialog;

    public KontakAdapter(Activity a, MatrixCursor mc, SimpleCursorAdapter adapter, String param) {
        this.activity = a;
        this.mMatrixCursor = mc;
        this.mAdapter = adapter;
        this.status = param;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Loading Kontak ...");
        progressDialog.show();
    }

    @Override
    protected Cursor doInBackground(Void... params) {
        if (status == null || status == ""){
            contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            contactsCursor = activity.getContentResolver().query(contactsUri, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        }else{
            contactsUri = ContactsContract.Contacts.CONTENT_URI;
            contactsCursor = activity.getContentResolver().query(
                    contactsUri,
                    null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " LIKE ?",
                    new String[]{"%"+status+"%"},
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        }
        if(contactsCursor.moveToFirst()){
            do{
                long contactId = contactsCursor.getLong(contactsCursor.getColumnIndex("_ID"));
                Uri dataUri = ContactsContract.Data.CONTENT_URI;
                Cursor dataCursor = activity.getContentResolver().query(dataUri, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
                String displayName="";
                String nickName="";
                String homePhone="";
                String mobilePhone="";
                String workPhone="";
                String photoPath="" + R.mipmap.ic_launcher;
                byte[] photoByte=null;
                String homeEmail="";
                String workEmail="";
                String companyName="";
                String title="";

                if(dataCursor.moveToFirst()){
                    // Getting Display Name
                    displayName = dataCursor.getString(dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME ));
                    do{
                        // Getting NickName
                        if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE))
                            nickName = dataCursor.getString(dataCursor.getColumnIndex("data1"));

                        // Getting Phone numbers
                        if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)){
                            switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
                                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME :
                                    homePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    break;
                                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE :
                                    mobilePhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    break;
                                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK :
                                    workPhone = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    break;
                            }
                        }
                        // Getting EMails
                        if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE ) ) {
                            switch(dataCursor.getInt(dataCursor.getColumnIndex("data2"))){
                                case ContactsContract.CommonDataKinds.Email.TYPE_HOME :
                                    homeEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    break;
                                case ContactsContract.CommonDataKinds.Email.TYPE_WORK :
                                    workEmail = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                                    break;
                            }
                        }
                        // Getting Organization details
                        if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE)){
                            companyName = dataCursor.getString(dataCursor.getColumnIndex("data1"));
                            title = dataCursor.getString(dataCursor.getColumnIndex("data4"));
                        }
                        // Getting Photo
                        if(dataCursor.getString(dataCursor.getColumnIndex("mimetype")).equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)){
                            photoByte = dataCursor.getBlob(dataCursor.getColumnIndex("data15"));
                            if(photoByte != null) {
                                Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);
                                // Getting Caching directory
                                File cacheDirectory = activity.getBaseContext().getCacheDir();
                                // Temporary file to store the activity_contact image
                                File tmpFile = new File(cacheDirectory.getPath() + "/wpta_"+contactId+".png");
                                // The FileOutputStream to the temporary file
                                try {
                                    FileOutputStream fOutStream = new FileOutputStream(tmpFile);
                                    // Writing the bitmap to the temporary file as png file
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOutStream);
                                    // Flush the FileOutputStream
                                    fOutStream.flush();
                                    //Close the FileOutputStream
                                    fOutStream.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                photoPath = tmpFile.getPath();
                            }
                        }
                    } while(dataCursor.moveToNext());

                    String details = "";
                    // Concatenating various information to single string
                    if(homePhone != null && !homePhone.equals("") )
                        details = "HomePhone : " + homePhone + "\n";
                    if(mobilePhone != null && !mobilePhone.equals("") )
                        details += "MobilePhone : " + mobilePhone + "\n";
                    if(workPhone != null && !workPhone.equals("") )
                        details += "WorkPhone : " + workPhone + "\n";
                    if(nickName != null && !nickName.equals("") )
                        details += "NickName : " + nickName + "\n";
                    if(homeEmail != null && !homeEmail.equals("") )
                        details += "HomeEmail : " + homeEmail + "\n";
                    if(workEmail != null && !workEmail.equals("") )
                        details += "WorkEmail : " + workEmail + "\n";
                    if(companyName != null && !companyName.equals("") )
                        details += "CompanyName : " + companyName + "\n";
                    if(title != null && !title.equals("") )
                        details += "Title : " + title + "\n";

                    // Adding id, display name, path to photo and other details to cursor
                    mMatrixCursor.addRow(new Object[]{Long.toString(contactId),displayName,photoPath,details});
                }
            }while(contactsCursor.moveToNext());
        }
        return mMatrixCursor;
    }

    @Override
    protected void onPostExecute(Cursor result) {
        // Setting the cursor containing contacts to listview
        progressDialog.dismiss();
        mAdapter.swapCursor(result);
    }
}
