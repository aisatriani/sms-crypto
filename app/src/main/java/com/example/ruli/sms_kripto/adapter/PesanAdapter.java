package com.example.ruli.sms_kripto.adapter;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ruli.sms_kripto.R;

import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;



public class PesanAdapter extends BaseAdapter {
    private RelativeLayout layout;
    private TextView id, pengirim, number, waktu, isi, count;
    private ImageView imgUser;
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;

    private Uri smsURI = Uri.parse("content://sms/inbox");

    public PesanAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        if(convertView==null) {
            view = inflater.inflate(R.layout.pesan_item, null);
        }

        layout = (RelativeLayout)view.findViewById(R.id.layoutPesan);
        id = (TextView)view.findViewById(R.id.txtPesanID);
        pengirim = (TextView)view.findViewById(R.id.txtPesanPengirim);
        number = (TextView)view.findViewById(R.id.txtPesanNomor);
        waktu = (TextView)view.findViewById(R.id.txtPesanWaktu);
        isi = (TextView)view.findViewById(R.id.txtPesanIsi);
        count = (TextView)view.findViewById(R.id.txtPesanCount);
        imgUser = (ImageView)view.findViewById(R.id.imgPesan);

        HashMap<String, String> inbox = new HashMap<String, String>();
        inbox = data.get(position);

        // Setting all values in listview
        id.setText(inbox.get("_id"));
        number.setText(getPhoneNumber(inbox.get("address")));
        pengirim.setText(getContactName(getPhoneNumber(inbox.get("address"))));
        waktu.setText(ConvertDate(inbox.get("date")));
        isi.setText(inbox.get("body"));
        count.setText(inbox.get("count")+" messages");

        customizeConv(inbox.get("_id"));

        return view;
    }

    private String ConvertDate(String date){
        Long timestamp = Long.parseLong(date);

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String finalDateString = formatter.format(calendar.getTime());

        return finalDateString;
    }

    private String getPhoneNumber(String rId){
        String phoneNumber = null;
        ContentResolver cr = activity.getContentResolver();
        Uri uri = Uri.parse("content://mms-sms/canonical-addresses");
        Cursor c = cr.query(uri, null, "_id = "+cekNomorGroup(rId), null, null);
        if (c.moveToNext()){
            phoneNumber = c.getString(1);
        }
        return phoneNumber;
    }

    private String getContactName(String phoneNumber) {
        String contactName = null;
        try {
            ContentResolver cr = activity.getContentResolver();
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
            Cursor cursor = cr.query(uri, new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
            if (cursor == null) {
                return null;
            }
            if (cursor.moveToFirst()) {
                contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
            }else{
                contactName = phoneNumber;
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }catch (IllegalArgumentException e){
            contactName = phoneNumber;
        }
        return contactName;
    }

    private void customizeConv(String id){
        ContentResolver cr = activity.getContentResolver();
        Cursor c = cr.query(smsURI, null, "thread_id = "+id+" AND read = 0", null, null);
        if (c.moveToFirst()) {
            layout.setBackgroundResource(R.color.colorPrimarySlow);
        }
    }

    private String cekNomorGroup(String text){
        String hasil = null;
        String[] items = text.split(" ");
        List<String> itemList = Arrays.asList(items);
        if (itemList.size() > 1){
            hasil = itemList.get(0);
        }else{
            hasil = text;
        }
        return hasil;
    }
}
