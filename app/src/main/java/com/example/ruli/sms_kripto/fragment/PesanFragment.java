package com.example.ruli.sms_kripto.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ruli.sms_kripto.PercakapanActivity;
import com.example.ruli.sms_kripto.Pref;
import com.example.ruli.sms_kripto.R;
import com.example.ruli.sms_kripto.SetPinActivity;
import com.example.ruli.sms_kripto.adapter.PesanAdapter;
import com.example.ruli.sms_kripto.adapter.VigenereCipherAdapter;
import com.greysonparrelli.permiso.Permiso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class PesanFragment extends Fragment {
    private static final String TAG = PesanFragment.class.getSimpleName();
    private ListView listView;
    private TextView ID, PENGIRIM, NOMOR;
    private PesanAdapter pesanAdapter;

    private String strUriInbox = "content://mms-sms/conversations?simple=true";

    private VigenereCipherAdapter vigenereCipherAdapter;

    public PesanFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vigenereCipherAdapter = new VigenereCipherAdapter();
        getActivity().setTitle("Pesan");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_pesan, container, false);

        setHasOptionsMenu(true);

        listView = (ListView)view.findViewById(R.id.listPesan);

        ambilPesan();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_lock, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_setpin){
            Intent intent = new Intent(getActivity(), SetPinActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }

    private void ambilPesan(){
        try{
            Uri uriSmsConversations = Uri.parse(strUriInbox);
            Cursor cursor = getActivity().getContentResolver().query(uriSmsConversations, null, null, null, "date DESC");
            if (cursor.moveToFirst()) {
                ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
                do {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("_id", cursor.getString(cursor.getColumnIndex("_id")));
                    data.put("date", cursor.getString(cursor.getColumnIndex("date")));
                    data.put("count", cursor.getString(cursor.getColumnIndex("message_count")));
                    data.put("address", cursor.getString(cursor.getColumnIndex("recipient_ids")));

                    String bodyDecrypt = vigenereCipherAdapter.decrypt( cursor.getString(cursor.getColumnIndex("snippet")), Pref.KEY_SMS);
                    Log.d(TAG, "getMessage body: "+ bodyDecrypt);
                    if(bodyDecrypt.startsWith(Pref.CODE_TAG_ENC)){
                        String[] bodySplit = bodyDecrypt.split(Pattern.quote(":"));
                        data.put("body", bodySplit[1]);
                    }else{
                        data.put("body", cursor.getString(cursor.getColumnIndex("snippet")));
                    }

                    //data.put("body", cursor.getString(cursor.getColumnIndex("snippet")));

                    dataList.add(data);
                }while (cursor.moveToNext());

                pesanAdapter = new PesanAdapter(getActivity(), dataList);
                listView.setAdapter(pesanAdapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ID = (TextView)view.findViewById(R.id.txtPesanID);
                        PENGIRIM = (TextView)view.findViewById(R.id.txtPesanPengirim);
                        NOMOR = (TextView)view.findViewById(R.id.txtPesanNomor);

                        Intent intent = new Intent(getActivity(), PercakapanActivity.class);
                        intent.putExtra("id_thread", ID.getText());
                        intent.putExtra("pengirim", PENGIRIM.getText());
                        intent.putExtra("nomor", NOMOR.getText());
                        startActivity(intent);
                    }
                });
                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        NOMOR = (TextView)view.findViewById(R.id.txtPesanNomor);
                        dialogHapusPesan(NOMOR.getText().toString());
                        return true;
                    }
                });
            }else{
                listView.setAdapter(null);
            }
        }catch (SQLiteException e){
            Toast.makeText(getActivity().getApplicationContext(), "Can't Read SMS Group", Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogHapusPesan(final String no){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Hapus Pesan");
        dialog.setMessage("Anda yakin akan menghapus pesan ini ?");
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteMessage(no);
            }
        });
        dialog.show();
    }

    private void deleteMessage(String no){
        String numberFilter = "address='"+ no + "'";
        String messageid = null;
        Cursor cursor = getActivity().getContentResolver().query(Uri.parse("content://sms/"),null, numberFilter, null, null);
        if (cursor.moveToFirst()) {
            messageid = cursor.getString(1);
            getActivity().getContentResolver().delete(Uri.parse("content://sms/"), "thread_id = "+messageid, null);
        }
        ambilPesan();
    }
}
