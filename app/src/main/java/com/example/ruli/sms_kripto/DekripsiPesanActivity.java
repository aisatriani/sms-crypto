package com.example.ruli.sms_kripto;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ruli.sms_kripto.adapter.VigenereCipherAdapter;

public class DekripsiPesanActivity extends AppCompatActivity {
    private String ID, PENGIRIM, NOMOR, ISI;
    private TextView txtHasil;
    private VigenereCipherAdapter vigenereCipherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dekripsi_pesan);

        getDataIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        vigenereCipherAdapter = new VigenereCipherAdapter();

        txtHasil = (TextView) findViewById(R.id.txtDekripsiHasil);

        dialogKunci();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogKunci();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                kembali();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public void onBackPressed(){
        kembali();
    }

    private void getDataIntent(){
        Intent intent = getIntent();
        ID = intent.getStringExtra("id_thread");
        PENGIRIM = intent.getStringExtra("pengirim");
        NOMOR = intent.getStringExtra("nomor");
        ISI = intent.getStringExtra("isi");
    }

    private void kembali(){
        Intent intent = new Intent(DekripsiPesanActivity.this, PercakapanActivity.class);
        intent.putExtra("id_thread", ID);
        intent.putExtra("pengirim", PENGIRIM);
        intent.putExtra("nomor", NOMOR);
        startActivity(intent);
    }

    private void dialogKunci(){
        final Dialog dialog = new Dialog(DekripsiPesanActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_kunci);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        final EditText key = (EditText) dialog.findViewById(R.id.txtDialogKunci);
        Button ok = (Button) dialog.findViewById(R.id.btnDialogOK);
        Button cancel = (Button) dialog.findViewById(R.id.btnDialogCancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Input Your Key !", Toast.LENGTH_SHORT).show();
                }else{
                    try {
                        txtHasil.setText(vigenereCipherAdapter.decrypt(ISI, key.getText().toString()));
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "INVALID PASSWORD", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
