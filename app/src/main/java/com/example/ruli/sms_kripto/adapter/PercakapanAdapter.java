package com.example.ruli.sms_kripto.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ruli.sms_kripto.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;



public class PercakapanAdapter extends BaseAdapter {
    private LinearLayout layout;
    private TextView id, waktu, isi, status, type;
    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;

    public PercakapanAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view=convertView;
        if(convertView==null)
            view = inflater.inflate(R.layout.percakapan_item, null);

        layout = (LinearLayout)view.findViewById(R.id.layoutPercakapan);
        id = (TextView)view.findViewById(R.id.txtPercakapanID);
        waktu = (TextView)view.findViewById(R.id.txtPercakapanWaktu);
        isi = (TextView)view.findViewById(R.id.txtPercakapanIsi);
        status = (TextView)view.findViewById(R.id.txtPercakapanStatus);
        type = (TextView)view.findViewById(R.id.txtPercakapanType);

        HashMap<String, String> inbox = new HashMap<String, String>();
        inbox = data.get(position);

        id.setText(inbox.get("_id"));
        waktu.setText(ConvertDate(inbox.get("date")));
        isi.setText(inbox.get("body"));
        status.setText(inbox.get("read"));
        type.setText(inbox.get("type"));

        setBubbleChat(inbox.get("type"));

        return view;
    }

    private String ConvertDate(String date){
        Long timestamp = Long.parseLong(date);

        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String finalDateString = formatter.format(calendar.getTime());

        return finalDateString;
    }

    private void setBubbleChat(String tipe){
        if (tipe.equals("1")){
            isi.setBackgroundResource(R.color.colorGrey);
            layout.setGravity(Gravity.LEFT);
        }else{
            isi.setBackgroundResource(R.color.colorPrimarySlow);
            layout.setGravity(Gravity.RIGHT);
        }
    }
}
