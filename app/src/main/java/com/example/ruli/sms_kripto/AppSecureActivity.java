package com.example.ruli.sms_kripto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by azisa on 7/29/2017.
 */

public class AppSecureActivity extends AppCompatActivity {

    private static final String TAG = AppSecureActivity.class.getSimpleName();
    private Pref pref;


    @Override
    protected void onRestart() {

        Log.e(TAG, "onresume: on restart appsecure");

        super.onRestart();

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
        if(pref.isLocked() && pref.getPin() != 0){
            showPinActivity();
        }

    }


    @Override
    protected void onPause() {

        Log.e(TAG, "onPause: activity on pause");
        super.onPause();

//        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
//        if(pref.getPin() != 0 && !pref.isLocked()){
//            setLock(true);
//        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: on create call");
        super.onCreate(savedInstanceState);
        System.out.println("create app secure");
        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));
        System.out.println(pref.isLocked());
        if(pref.isLocked() && pref.getPin() != 0){
            showPinActivity();
        }
    }

    public void showPinActivity() {
        Intent intent = new Intent(this, PinLockActivity.class);
        startActivityForResult(intent,200);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 200){
            Log.e(TAG, "onActivityResult: result from pin" );
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("set lock app after destroy activity");
    }

    public void setLock(boolean lock){
        pref.setLocked(lock);
    }
}
