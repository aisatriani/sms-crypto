package com.example.ruli.sms_kripto.adapter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.telephony.gsm.SmsMessage;

import com.example.ruli.sms_kripto.PercakapanActivity;
import com.example.ruli.sms_kripto.R;

import java.io.ByteArrayInputStream;


public class ReceiveAdapter extends BroadcastReceiver {
    public static final String SMS_BUNDLE = "pdus";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle intentExtras = intent.getExtras();
        if (intentExtras != null) {
            String address = null;
            String smsBody = null;
            Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
            for (int i = 0; i < sms.length; ++i) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);
                smsBody = smsMessage.getMessageBody().toString();
                address = smsMessage.getOriginatingAddress();
            }
            Notification(context, address, smsBody);
        }
    }

    public void Notification(Context context, String address, String body) {
        Intent intent = new Intent(context.getApplicationContext(), PercakapanActivity.class);
        intent.putExtra("id_thread", getTreadID2(context, address));
        intent.putExtra("pengirim", getContactName(context, address));
        intent.putExtra("nomor", address);

        PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context.getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(openPhoto(context.getApplicationContext(), getContactsID(context.getApplicationContext(), address)))
                .setTicker(getContactName(context, address))
                .setContentTitle(getContactName(context, address))
                .setContentText(body)
                .addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
                .setContentIntent(pIntent)
                .setAutoCancel(true);

        NotificationManager notificationmanager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationmanager.notify(0, builder.build());
    }

    private String getTreadID2(Context c, String address){
        String numberFilter = "address='"+ address + "'";
        String threadID = null;
        Cursor cursor = c.getApplicationContext().getContentResolver().query(Uri.parse("content://sms/"), null, numberFilter, null, null);
        if (cursor.moveToFirst()) {
            threadID = cursor.getString(1);
        }else{
            threadID = "";
        }
        return threadID;
    }

    private String getContactName(Context c, String phoneNumber) {
        ContentResolver cr = c.getApplicationContext().getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }else{
            contactName = phoneNumber;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }

    private long getContactsID(Context c, String address) {
        long contactId = 0;
        Cursor cursor = c.getApplicationContext().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                " data1 = ?",
                new String[]{address},
                null);
        if (cursor.moveToFirst()) {
            try{
                contactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            }catch (NullPointerException e){
                contactId = 0;
            }
        } else {
            contactId = 0;
        }
        return contactId;
    }

    private Bitmap openPhoto(Context c, long contactId) {
        Bitmap bm;
        if (contactId == 0){
            bm = BitmapFactory.decodeResource(c.getApplicationContext().getResources(), R.mipmap.ic_user);
        }else {
            Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
            Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
            Cursor cursor = c.getApplicationContext().getContentResolver().query(photoUri, new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
            if (cursor == null) {
                bm = BitmapFactory.decodeResource(c.getApplicationContext().getResources(), R.mipmap.ic_user);
            }
            try {
                if (cursor.moveToFirst()) {
                    byte[] data = cursor.getBlob(0);
                    if (data != null) {
                        bm = BitmapFactory.decodeStream(new ByteArrayInputStream(data));
                    }else{
                        bm = BitmapFactory.decodeResource(c.getApplicationContext().getResources(), R.mipmap.ic_user);
                    }
                } else {
                    bm = BitmapFactory.decodeResource(c.getApplicationContext().getResources(), R.mipmap.ic_user);
                }
            }finally {
                cursor.close();
            }
        }
        return bm;
    }
}
