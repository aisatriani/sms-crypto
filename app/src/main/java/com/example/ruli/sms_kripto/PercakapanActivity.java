package com.example.ruli.sms_kripto;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ruli.sms_kripto.adapter.PercakapanAdapter;
import com.example.ruli.sms_kripto.adapter.VigenereCipherAdapter;
import com.example.ruli.sms_kripto.adapter.PercakapanAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class PercakapanActivity extends AppSecureActivity {
    private static final String TAG = PercakapanActivity.class.getSimpleName();
    private String ID, PENGIRIM, NOMOR, KUNCI;
    private PercakapanAdapter percakapanAdapter;
    private ListView listView;
    private ImageView btnKirim;
    private EditText isi;

    private int STATUS = 1;
    private Uri smsURI = Uri.parse("content://sms");

    private VigenereCipherAdapter vigenereCipherAdapter;

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
       super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_percakapan);

        getDataIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(PENGIRIM);

        vigenereCipherAdapter = new VigenereCipherAdapter();

        listView = (ListView) findViewById(R.id.listPercakapan);
        btnKirim = (ImageView) findViewById(R.id.imgPercakapanKirim);
        isi = (EditText) findViewById(R.id.txtPercakapanTulisPesan);

        if(ID != null)
        if (!ID.equals("")){
            getMessage(ID);
            markRead();
        }else{
            listView.setAdapter(null);
        }

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isi.length() > 0) {
                    kirimPesan(NOMOR, isi.getText().toString());
                    isi.setText(null);
                } else {
                    Toast.makeText(getBaseContext(), "Masukan Pesan Anda !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                TextView isi = (TextView) view.findViewById(R.id.txtPercakapanIsi);
//                Intent intent = new Intent(PercakapanActivity.this, DekripsiPesanActivity.class);
//                intent.putExtra("id_thread", ID);
//                intent.putExtra("pengirim", PENGIRIM);
//                intent.putExtra("nomor", NOMOR);
//                intent.putExtra("isi", isi.getText().toString());
//                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_enkrip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_enkrip) {
            if (STATUS == 0){
                dialogKunci(item);
            }else{
                Toast.makeText(getApplicationContext(), "Pesan Tidak Di Enkripsi !", Toast.LENGTH_LONG).show();
                item.setIcon(R.mipmap.ic_open);
                STATUS = 0;
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getDataIntent(){
        Intent intent = getIntent();
        ID = intent.getStringExtra("id_thread");
        PENGIRIM = intent.getStringExtra("pengirim");
        NOMOR = intent.getStringExtra("nomor");
    }

    private void getMessage(String newIdThread){
        String[] reqCols = new String[] {"_id", "date", "body", "read", "type"};
        ContentResolver cr = getContentResolver();
        Cursor c = null;
        if (newIdThread.equals("")){
            c = cr.query(smsURI, reqCols, null, null, "date ASC");
        }else {
            c = cr.query(smsURI, reqCols, "thread_id = " + newIdThread, null, "date ASC");
        }
        if (c.moveToFirst()) {
            ArrayList<HashMap<String, String>> inboxList = new ArrayList<HashMap<String, String>>();
            do {
                HashMap<String, String> data = new HashMap<>();

                data.put("_id", c.getString(0));
                data.put("date", c.getString(1));

                String bodyDecrypt = vigenereCipherAdapter.decrypt( c.getString(2), Pref.KEY_SMS);
                Log.d(TAG, "getMessage body: "+ bodyDecrypt);
                if(bodyDecrypt.startsWith(Pref.CODE_TAG_ENC)){
                    String[] bodySplit = bodyDecrypt.split(Pattern.quote(":"));
                    data.put("body", bodySplit[1]);
                }else{
                    data.put("body", c.getString(2));
                }
                //data.put("body", c.getString(2));
                data.put("read", c.getString(3));
                data.put("type", c.getString(4));

                inboxList.add(data);
            } while (c.moveToNext());

            percakapanAdapter = new PercakapanAdapter(PercakapanActivity.this, inboxList);
            listView.setAdapter(percakapanAdapter);
            listView.setSelection(listView.getAdapter().getCount() - 1);
        } else {
            listView.setAdapter(null);
        }
    }

    private void markRead(){
        Uri smsURI = Uri.parse("content://sms/inbox");
        ContentValues values = new ContentValues();
        values.put("read", true);
        ContentResolver cr = getContentResolver();
        cr.update(smsURI, values, "thread_id ="+ID, null);
    }

    private void dialogKunci(final MenuItem item){
        final Dialog dialog = new Dialog(PercakapanActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_kunci);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);

        final EditText key = (EditText) dialog.findViewById(R.id.txtDialogKunci);
        Button ok = (Button) dialog.findViewById(R.id.btnDialogOK);
        Button cancel = (Button) dialog.findViewById(R.id.btnDialogCancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (key.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Masukan Kunci Anda !", Toast.LENGTH_SHORT).show();
                }else{
                    KUNCI = key.getText().toString();
                    STATUS = 1;
                    item.setIcon(R.mipmap.ic_lock);
                    Toast.makeText(getApplicationContext(), "Pesan Akan Di Enkripsi !", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void kirimPesan(String phoneNumber, String message){
        String nMessage = null;
        if (STATUS == 1){
            message = Pref.CODE_TAG_ENC + message.toLowerCase();

            nMessage = vigenereCipherAdapter.encrypt(message, Pref.KEY_SMS);

            }
        else{
            nMessage = message;
        }
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "Pesan Terkirim", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "Pesan Di Terima", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "Pesan Tidak Di Terima", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, nMessage, sentPI, deliveredPI);

        getTreadID(phoneNumber);
    }

    private void getTreadID(String address){
        String numberFilter = "address='"+ address + "'";
        Cursor cursor = getContentResolver().query(smsURI, null, numberFilter, null, null);
        if (cursor.moveToFirst()) {
            String threadID = cursor.getString(1);
            getMessage(threadID);
        }else{
            getMessage("");
        }
    }


}
