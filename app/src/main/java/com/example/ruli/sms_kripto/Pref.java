package com.example.ruli.sms_kripto;

import android.content.SharedPreferences;

/**
 * Created by azisa on 7/29/2017.
 */

public class Pref {

    public static final String CODE_TAG_ENC = "sc:";
    public static final String KEY_SMS = "qwe";
    private static final String KEY_PIN = "key.pin";
    private static final String KEY_LOCKED = "key.lock";
    public static final String PREF_NAME = "smscrypto.pref";
    private SharedPreferences preferences;

    public Pref(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void setPIN(int pin){
        SharedPreferences.Editor edit = preferences.edit();
        edit.putInt(KEY_PIN, pin);
        edit.apply();
    }

    public int getPin(){
        int pin = preferences.getInt(KEY_PIN, 0);
        return pin;
    }

    public void setLocked(boolean lock){
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(KEY_LOCKED, lock);
        edit.apply();
    }

    public boolean isLocked(){
        boolean isLocked = preferences.getBoolean(KEY_LOCKED, true);
        return isLocked;
    }
}
