package com.example.ruli.sms_kripto;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.ruli.sms_kripto.fragment.AboutFragment;
import com.example.ruli.sms_kripto.fragment.KontakFragment;
import com.example.ruli.sms_kripto.fragment.PesanFragment;
import com.greysonparrelli.permiso.Permiso;

public class MainActivity extends AppSecureActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private BottomNavigationView mBottomNav;
    private Pref pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("create main activity");
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Permiso.getInstance().setActivity(this);
        Permiso.getInstance().requestPermissions(
                new Permiso.IOnPermissionResult() {
                    @Override
                    public void onPermissionResult(Permiso.ResultSet resultSet) {
                        if (resultSet.isPermissionGranted(Manifest.permission.READ_CONTACTS)
                            && resultSet.isPermissionGranted(Manifest.permission.READ_SMS)
                            && resultSet.isPermissionGranted(Manifest.permission.RECEIVE_SMS)
                            && resultSet.isPermissionGranted(Manifest.permission.RECEIVE_MMS)
                            && resultSet.isPermissionGranted(Manifest.permission.SEND_SMS)) {
                                selectFragment(R.id.menu_kontak);
                        }
                    }
                    @Override
                    public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                        Permiso.getInstance().showRationaleInDialog("PERMISSON", "Tidak Di Izinkan !", null, callback);
                    }
                }, Manifest.permission.READ_CONTACTS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.RECEIVE_MMS,
                    Manifest.permission.SEND_SMS);

        mBottomNav = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item.getItemId());
                return true;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, KirimPesanAcitivty.class));
            }
        });
    }

    public void onBackPressed(){
        //set lock after close activity
        System.out.println("set lock app after close activity main");
        setLock(true);
        moveTaskToBack(true);
    }

    private void selectFragment(int id) {
        Fragment fragment = null;
        switch (id) {
            case R.id.menu_kontak:
                fragment = new KontakFragment();
                break;
            case R.id.menu_pesan:
                fragment = new PesanFragment();
                break;
            case R.id.menu_about:
                fragment = new AboutFragment();
                break;
        }

        if (fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_main, fragment);
            ft.commit();
        }
    }

}
