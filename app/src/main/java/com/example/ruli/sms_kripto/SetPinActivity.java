package com.example.ruli.sms_kripto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;


public class SetPinActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SetPinActivity.class.getSimpleName();
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private TextView textStatusLock;
    private Button btnSelesai;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);

        pref = new Pref(getSharedPreferences(pref.PREF_NAME, MODE_PRIVATE));

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mPinLockView.setPinLockListener(mPinLockListener);

        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        mPinLockView.attachIndicatorDots(mIndicatorDots);

        textStatusLock = (TextView) findViewById(R.id.text_status_lock);
        btnSelesai = (Button) findViewById(R.id.btn_selesai);
        btnSelesai.setOnClickListener(this);
    }

    private String lastPin;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            if( lastPin != null){
                if(lastPin.equals(pin)){
                    textStatusLock.setText("Pin berhasil di masukan. Sekarang aplikasi ini terlindungi.");
                    btnSelesai.setVisibility(View.VISIBLE);
                    pref.setPIN(Integer.valueOf(pin));
                    pref.setLocked(false);
                    return;
                }else{
                    textStatusLock.setText("Konfirmasi pin salah. Harap masukan kembali pin yg benar");
                    mPinLockView.resetPinLockView();
                    return;
                }
            }

            lastPin = pin;
            mPinLockView.resetPinLockView();
            textStatusLock.setText("Masukan sekali lagi pin anda untuk konfirmasi");
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_selesai){
            this.finish();
        }
    }
}
