package com.example.ruli.sms_kripto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;


public class PinLockActivity extends AppCompatActivity {

    private static final String TAG = PinLockActivity.class.getSimpleName();
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private Pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock);

        pref = new Pref(getSharedPreferences(Pref.PREF_NAME, MODE_PRIVATE));

        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mPinLockView.setPinLockListener(mPinLockListener);

        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        mPinLockView.attachIndicatorDots(mIndicatorDots);
    }

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            int enterPin = Integer.valueOf(pin);
            if(pref.getPin() == enterPin){
                setResult(200);
                pref.setLocked(false);
                PinLockActivity.this.finish();
            }else {
                mPinLockView.resetPinLockView();
                Toast.makeText(PinLockActivity.this, "Pin yang anda masukan salah. Ulangi kembali", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

}
