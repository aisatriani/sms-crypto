package com.example.ruli.sms_kripto.adapter;


public class VigenereCipherAdapter {
    public String encrypt(String plainText, String key) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < plainText.length(); i++) {
            // perulangan karakter
            char c = plainText.charAt(i);
            if (c >= 32) {

                int keyCharValue = key.charAt(i % key.length()) - 'A';
                c += keyCharValue;
                if (c > 126) {
                    //kembali ke karakter pertama jika > 126
                    c = (char) (c + 32 - 126);
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public String decrypt(String cipherText, String key){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cipherText.length(); i++) {
            char c = cipherText.charAt(i);
            if (c >= 32) {

                int keyCharValue = key.charAt(i % key.length()) - 'A';
                c -= keyCharValue;
                if (c < 32) {
                    //kembali ke karakter pertama jika < 32
                    c = (char) (c + 126 - 32);
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
