package com.example.ruli.sms_kripto.fragment;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.ruli.sms_kripto.PercakapanActivity;
import com.example.ruli.sms_kripto.R;
import com.example.ruli.sms_kripto.SetPinActivity;
import com.greysonparrelli.permiso.Permiso;

public class KontakFragment extends Fragment{
    private ListView listView;
    private SimpleCursorAdapter adapter;
    private SearchView searchView;

    public static final int CONTACT_LOADER_ID = 78;
    private String cursorFilter;
    private Uri baseUri;

    public KontakFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_kontak, container, false);
        getActivity().setTitle("Kontak");
        setHasOptionsMenu(true);

        setupCursorAdapter();

        listView = (ListView) view.findViewById(R.id.listKontak);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView nomor = (TextView) view.findViewById(R.id.txtKontakNomor);
                TextView nama = (TextView) view.findViewById(R.id.txtKontakNama);
                Intent intent = new Intent(getActivity(), PercakapanActivity.class);
                intent.putExtra("id_thread", "");
                intent.putExtra("pengirim", nama.getText().toString());
                intent.putExtra("nomor", nomor.getText().toString());
                startActivity(intent);
            }
        });

        getActivity().getSupportLoaderManager().initLoader(CONTACT_LOADER_ID, new Bundle(), contactsLoader);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cari, menu);

        final MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                cursorFilter = !TextUtils.isEmpty(s) ? s : null;
                getActivity().getSupportLoaderManager().restartLoader(CONTACT_LOADER_ID, new Bundle(), contactsLoader);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        } else if(id == R.id.action_setpin){
            Intent intent = new Intent(getActivity(), SetPinActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupCursorAdapter() {
        String[] uiBindFrom = { ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        int[] uiBindTo = { R.id.txtKontakID,
                R.id.txtKontakNama,
                R.id.txtKontakNomor};

        adapter = new SimpleCursorAdapter(
                getActivity(), R.layout.kontak_item,
                null, uiBindFrom, uiBindTo,
                0);
    }

    private LoaderManager.LoaderCallbacks<Cursor> contactsLoader = new LoaderManager.LoaderCallbacks<Cursor>() {
                @Override
                public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                    if (cursorFilter != null) {
                        baseUri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(cursorFilter));
                    } else {
                        baseUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                    }

                    String select = "((" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " NOTNULL) AND ("
                            + ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER + " = 1) AND ("
                            + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " != '' ))";

                    String[] projectionFields = new String[] {
                            ContactsContract.CommonDataKinds.Phone._ID,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.NUMBER};

                    CursorLoader cursorLoader = new CursorLoader(getActivity(),
                            baseUri, // URI
                            projectionFields, // projection fields
                            select, // the selection criteria
                            null, // the selection args
                            null // the sort order
                    );

                    return cursorLoader;
                }

                @Override
                public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                    adapter.swapCursor(cursor);
                }

                @Override
                public void onLoaderReset(Loader<Cursor> loader) {
                    adapter.swapCursor(null);
                }
            };
}
